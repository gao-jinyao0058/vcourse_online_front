import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper/dist/ssr'
import ElementUI from 'element-ui'

import 'element-ui/lib/theme-chalk/index.css'


Vue.use(ElementUI)
Vue.use(VueAwesomeSwiper)
