import axios from 'axios'
import {Message, MessageBox} from 'element-ui'
import Cookie from 'js-cookie'

// 创建axios实例
const service = axios.create({
  baseURL: 'http://localhost:10086', // api 的 base_url
  timeout: 10000 // 请求超时时间
})

// http request 拦截器
service.interceptors.request.use(
  config => {
    // debugger
    if (Cookie.get('user-token')) {
      config.headers['token'] = Cookie.get('user-token');
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  }
)

// response 拦截器
service.interceptors.response.use(
  response => {
    /**
     * code为非20000是抛错 可结合自己业务进行修改
     */
    const res = response.data
    if (res.code !== 20000) {
      Message({
        message: res.message,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject('error')
    } else {
      return response.data
    }
  }
)

export default service
