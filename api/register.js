import request from '@/utils/request'

/* 用户名检测方法 */
export function getUserByUsername(username) {
  return request({
    url: `/usercenter/user/getUserByUsername/${username}`,
    method: 'get'
  })
}

/* 用户注册方法 */
export function userRegister(user) {
  return request({
    url: `/usercenter/user/register`,
    method: 'put',
    data: user
  })
}
