import request from '@/utils/request'

/* banner 列表数据查询方法 */
export function getBannerList() {
  return request({
    url: `/banner/front/getBanners`,
    method: 'get'
  })
}
