import request from '@/utils/request'

/* 首页课程列表数据查询方法 */
export function getIndexTeacher() {
  return request({
    url: `/edu/teacher/front/getTeachers/1/4`,
    method: 'get'
  })
}
