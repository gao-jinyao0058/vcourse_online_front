import request from '@/utils/request'

/* 首页课程列表数据查询方法 */
export function getIndexCourse() {
  return request({
    url: `/edu/course/front/getCourse/1/8`,
    method: 'get'
  })
}
/* 课程列表分页数据查询方法 */
export function getCoursePage(current,size,searchObj) {
  return request({
    url: `/edu/course/front/getCourse/${current}/${size}`,
    method: 'post',
    data:searchObj
  })
}
/* 课程列表所有分类数据查询方法 */
export function getAllSubject() {
  return request({
    url: `/edu/subject/getAllSubject`,
    method: 'get'
  })
}
/* 课程详情查询方法 */
export function getCourseById(courseId) {
  return request({
    url: `/edu/course/front/getCourseById/${courseId}`,
    method: 'get'
  })
}




