import request from '@/utils/request'

/* 课程详情查询方法 */
export function getPlayAuthByVideoId(videoId) {
  return request({
    url: `/vod/getPlayAuth/${videoId}`,
    method: 'get'
  })
}
