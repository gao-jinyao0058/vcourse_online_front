import request from '@/utils/request'

/* 用户登录方法 */
export function userLogin(user) {
  return request({
    url: `/usercenter/user/login`,
    method: 'post',
    data: user
  })
}

/* 根据 token 获取用户信息方法 */
export function getLoginUser() {
  return request({
    url: `/usercenter/user/auth/getLoginUser`,
    method: 'get'
  })
}
